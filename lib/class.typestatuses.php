<?php

class TypeStatuses extends BaseClass
{
    private $this_category = "type-status";
    protected $job_name = "type-statuses";

    public function __construct ()
    {
        parent::__construct();
        $this->logger->setCallingClassOverride(get_class());
        $this->setCategory($this->this_category);
    }
}