<?php

class Taxa extends BaseClass
{
    private $document_limit = -1;
    private $this_category = "taxon";
    protected $job_name = "taxon";
    private $multimedia=[];
    private $xenocanto_link_text = "Hoe klinkt deze vogel?";
    private $natuurwijzer_links_limit = 5;
    private $skipped_docs = 0;
    private $written_docs = 0;
    private $failed_docs = 0;
    private $buffer_size = 10000;
    private $ttik_descriptions = [];
    private $nba_taxa_limit = -1;

    public function __construct ()
    {
        parent::__construct();
        $this->logger->setCallingClassOverride(get_class());
        $this->setCategory($this->this_category);
    }

    public function generate()
    {
        $this->setNatuurwijzerColumn("taxon");
        $this->setNatuurwijzerLinks();
        $this->setXenocantoLinks();
        $this->setTtikDescriptions();
        $this->setTtikTaxa();

        $this->processTtikTaxa();
        $this->processNbaTaxa();
        $this->processRestLemmas();

        $this->setJobResult([
            "files" => $this->written_docs,
            "skipped (lack of content)" => $this->skipped_docs
        ]);

        $this->logger->log("wrote " . number_format($this->written_docs) . " documents");
        $this->logger->log("skipped " . number_format($this->skipped_docs) . " because of lack of content");
        $this->logger->log("failed to write " . number_format($this->failed_docs));
        $this->logger->log("finished");
    }

    public function setDocumentLimit($document_limit)
    {
        $this->document_limit = $document_limit;
        $this->logger->log("document limit: $this->document_limit");
    }

    public function setNbaTaxaLimit($nba_taxa_limit)
    {
        $this->nba_taxa_limit = $nba_taxa_limit;
        $this->logger->log("nba taxa limit: " . number_format($this->nba_taxa_limit));
    }

    public function doDeleteExisting()
    {
        if ($this->delete_existing)
        {
            $empty = $this->export_basepath . "empty/";
            mkdir($empty);
            exec("rsync -a --delete " . $empty ." " . $this->export_path, $output, $retval);
            rmdir($empty);
            $this->logger->log("deleted old files; rsync exit code: $retval");
        }
    }

    protected function setXenocantoLinks()
    {
        $stmt = $this->db->prepare("select * from xenocanto where nomen is not null");
        $result = $stmt->execute();

        while ($res = $result->fetchArray(SQLITE3_ASSOC))
        {
            $xc = json_decode($res["sound"],true);

            $this->multimedia[] = [
                "taxon" =>
                    [
                        "scientific_name" => $res["scientific_name"],
                        "nomen" => $res["nomen"],
                    ],
                "multimedia" =>
                    [
                        "url" => $xc["accessUri"],
                        "text" => $this->xenocanto_link_text,
                        "type" => $xc["format"],
                        "owner" => $xc["owner"],
                        "license" => $xc["license"],
                        "source" => $xc["recordURI"],
                    ]
                ];
        }

        $this->logger->log("found " . number_format(count($this->multimedia)) . " links from XenoCanto");
    }

    protected function setTtikDescriptions()
    {

        $stmt = $this->db->prepare("
            select
                _a.content,
                _c.title,
                lower(trim(_e.uninomial || ' ' || ifnull(_e.specific_epithet,'') || ' '|| ifnull(_e.infra_specific_epithet,''))) as nomen

            from
                content_taxa _a

            left join pages_taxa _b
                on _a.project_id = _b.project_id
                and _a.page_id = _b.id

            left join pages_taxa_titles _c
                on _a.project_id = _c.project_id
                and _a.page_id = _c.page_id
                and _a.language_id = _c.language_id

            left join taxa _d
                on _a.project_id = _d.project_id
                and _a.taxon_id = _d.id

            left join name_types _f
                on _a.project_id = _f.project_id

            left join names _e
                on _a.project_id = _e.project_id
                and _a.taxon_id = _e.taxon_id
                and _e.type_id = _f.id

            where
                _a.language_id = :language_id
                and _a.content != ''
                and _b.page in ('Description','Leefgebied','Leefperiode')
                and _f.nametype = 'isValidNameOf'
            order by _d.taxon, _c.title
        ");

        $stmt->bindValue(':language_id',$this->ttik_language_id,SQLITE3_TEXT);
        $result = $stmt->execute();

        while ($res = $result->fetchArray(SQLITE3_ASSOC))
        {
            $this->ttik_descriptions[$res["nomen"]][] = [
                "title" => trim($this->clearHtml($res["title"])),
                "body" => trim($this->clearHtml($res["content"]))
            ];
        }
    }

    protected function setTtikTaxa()
    {
        $stmt = $this->db->prepare("
            select
                _a.taxon,
                _a.id as taxon_id,
                _a.parent_id,
                _b.uninomial, _b.specific_epithet, _b.infra_specific_epithet, _b.authorship,
                lower(trim(_b.uninomial || ' ' || ifnull(_b.specific_epithet,'') || ' '|| ifnull(_b.infra_specific_epithet,''))) as nomen,
                _r.rank,
                'TTIK' as _source

            from
                taxa _a

            left join name_types _f
                on _a.project_id = _f.project_id

            left join names _b
                on _a.project_id = _b.project_id
                and _a.id = _b.taxon_id
                and _b.type_id = _f.id

            left join projects_ranks _p
                on _a.project_id = _b.project_id
                and _a.rank_id = _p.id

            left join ranks _r
                on _p.rank_id = _r.id

            where
                _f.nametype = 'isValidNameOf'

            order by
                _a.taxon
        ");

        $result = $stmt->execute();

        while ($res = $result->fetchArray(SQLITE3_ASSOC))
        {

            if ($this->document_limit > 0 && (($this->skipped_docs + $this->written_docs + $this->failed_docs) >= $this->document_limit))
            {
                break;
            }

            $record = [
                "classification" => [
                    [
                        "taxon" => $res["taxon"],
                        "rank" => $res["rank"],
                        "_parent_id" => $res["parent_id"],
                        "nomen" => $res["nomen"]
                    ]
                ],
                "uninomial" => $res["uninomial"],
                "specific_epithet" => $res["specific_epithet"],
                "infraspecific_epithet" => $res["infra_specific_epithet"],
                "authorship" => $res["authorship"],
                "taxon" => $res["taxon"],
                "nomen" => $res["nomen"],
                "rank" => $res["rank"],
                "_source" => "TTIK",
                "_taxon_id" => $res["taxon_id"],
                "_parent_id" => $res["parent_id"],
            ];

            $this->lemma_data[] = $record;

        }

        foreach ($this->lemma_data as $key => $val)
        {
            $this->lemma_data[$key]["classification"] = json_encode($this->buildClassification($val["classification"]));
        }

        $this->logger->log("found " . number_format(count($this->lemma_data)) . " taxa from TTIK");
    }

    private function buildClassification($classification)
    {
        $matches = array_filter($this->lemma_data,function($a) use ($classification)
        {
            return $a["_taxon_id"]==$classification[0]["_parent_id"];
        });

        if (count($matches)>0)
        {
            $match = array_pop($matches);

            $c = [
                "taxon" => $match["taxon"],
                "rank" => $match["rank"],
                "_parent_id" => $match["_parent_id"]
            ];

            if (isset($match["authorship"]))
            {
                $c["authorship"] = $match["authorship"];
            }

            array_unshift($classification,$c);

            return $this->buildClassification($classification);
        }
        else
        {
            return $classification;
        }
    }

    protected function processTtikTaxa()
    {
        $this->addNatuurwijzerLinks();
        $this->addTtikDescriptions();
        $this->generateJsonDocuments();
        $this->lemma_data=[];
    }

    protected function processNbaTaxa()
    {
        $stmt = $this->db->prepare("select count(*) as total from nba_taxa where taxon not in (select taxon from taxa)");
        $result = $stmt->execute();
        $res = $result->fetchArray(SQLITE3_ASSOC);
        $this->logger->log(
            "found " .
            number_format($this->nba_taxa_limit > -1 ? min([$this->nba_taxa_limit,$res["total"]]) : $res["total"] ).
            " taxa from NBA not in TTIK");

        $stmt = $this->db->prepare("select * from nba_taxa where taxon not in (select taxon from taxa) limit :limit");


        $stmt->bindValue(':limit',$this->nba_taxa_limit,SQLITE3_INTEGER);
        $result = $stmt->execute();

        while ($res = $result->fetchArray(SQLITE3_ASSOC))
        {
            if ($this->document_limit > 0 && (($this->skipped_docs + $this->written_docs + $this->failed_docs) >= $this->document_limit))
            {
                break;
            }

            $this->lemma_data[] = [
                "classification" => $res["classification"],
                "uninomial" => $res["uninomial"],
                "specific_epithet" => $res["specific_epithet"],
                "infraspecific_epithet" => $res["infraspecific_epithet"],
                "authorship" => $res["authorship"],
                "taxon" => $res["taxon"],
                "nomen" => trim(implode(" ",[$res["uninomial"],$res["specific_epithet"],$res["infraspecific_epithet"]])),
                "rank" => $res["rank"],
                "_source" => "NBA"
            ];

            if (count($this->lemma_data)>=$this->buffer_size)
            {
                $this->addNatuurwijzerLinks();
                $this->addTtikDescriptions();
                $this->generateJsonDocuments();
                $this->lemma_data=[];
            }
        }
    }

    private function processRestLemmas()
    {
        if (count($this->lemma_data)>0)
        {
            $this->addNatuurwijzerLinks();
            $this->addTtikDescriptions();
            $this->generateJsonDocuments();
            $this->lemma_data=[];
        }
    }

    private function addNatuurwijzerLinks()
    {
        foreach($this->lemma_data as $key => $val)
        {
            if (!isset($val["classification"]))
            {
                $this->logger->log("no classification: " . $val["taxon"]);
                continue;
            }

            $class = @json_decode($val["classification"],true);

            if ($class===null)
            {
                $this->logger->log("classification decode error: " . $val["taxon"] . " (" . $this->getLastJsonError() .")");
                continue;
            }

            $links=[];

            foreach(array_reverse($class) as $node)
            {
                $matches = array_filter($this->links,
                    function($a,$b) use ($node)
                    {
                        return
                            (strtolower(trim($node["taxon"] . " "  . ($node["authorship"] ?? ""))) == strtolower($b)) ||
                            (strtolower($node["taxon"]) == strtolower($b));
                    },
                    ARRAY_FILTER_USE_BOTH);

                if ($matches)
                {
                    $matches = array_pop($matches);
                    $links = array_merge($links,$matches);
                }
            }

            $b=[];
            foreach ($links as $val)
            {
                $b[$val["url"]] = $val;
            }

            // shuffle($b);

            $this->lemma_data[$key]["natuurwijzer_links"]=array_values($b);
        }
    }

    private function addTtikDescriptions()
    {
        foreach($this->lemma_data as $key => $val)
        {
            if (!isset($val["classification"]))
            {
                $this->logger->log("no classification: " . $val["taxon"]);
                continue;
            }

            $class = @json_decode($val["classification"],true);

            if ($class===null)
            {
                $this->logger->log("classification decode error: " . $val["taxon"] . " (" . $this->getLastJsonError() .")");
                continue;
            }

            foreach(array_reverse($class) as $node)
            {
                if (isset($node["nomen"]) && isset($this->ttik_descriptions[strtolower($node["nomen"])]))
                {
                    $this->lemma_data[$key]["description"]=$this->ttik_descriptions[strtolower($node["nomen"])];
                    break;
                }
                else
                if (isset($node["taxon"]) && isset($this->ttik_descriptions[strtolower($node["taxon"])]))
                {
                    $this->lemma_data[$key]["description"]=$this->ttik_descriptions[strtolower($node["taxon"])];
                    break;
                }
            }
        }
    }

    protected function generateJsonDocuments()
    {
        foreach($this->lemma_data as $key => $lemma)
        {
            if (
                $this->document_limit > 0 &&
                (
                    ($this->skipped_docs + $this->written_docs + $this->failed_docs) >= $this->document_limit
                )
            )
            {
                break;
            }

            $id = $this->makeDocId($lemma["nomen"]) . "-" . $this->language;
            $doc = $this->makeDoc($this->category,$this->language,$id,$lemma["taxon"]);
            $doc = $this->docAddContent($doc,$lemma);

            if (isset($lemma["description"]) && count($lemma["description"])>0)
            {
                $doc["content"] = $lemma["description"];
            }

            if (isset($lemma["natuurwijzer_links"]) && count($lemma["natuurwijzer_links"]) > 0)
            {
                $doc["links"] = array_slice($lemma["natuurwijzer_links"],0,$this->natuurwijzer_links_limit);
            }

            if ((isset($lemma["taxon"]) || isset($lemma["nomen"])) && !empty($this->multimedia))
            {
                $matches = array_filter(
                    $this->multimedia,
                    function($a) use ($lemma)
                    {
                        if (isset($a["taxon"]["scientific_name"]) && isset($lemma["taxon"]))
                        {
                            return $a["taxon"]["scientific_name"] == $lemma["taxon"];
                        }
                        else
                        if (isset($a["taxon"]["nomen"]) && isset($lemma["nomen"]))
                        {
                            return $a["taxon"]["nomen"] == $lemma["nomen"];
                        }

                        return false;
                    });

                if (!empty($matches))
                {
                    foreach ($matches as $match)
                    {
                        $doc["multimedia"][] = $match["multimedia"];
                    }
                }
            }

            if (empty($doc["content"]) && empty($doc["links"]) && empty($doc["multimedia"]) && !$this->generate_empty)
            {
                $this->skipped_docs++;
            }
            else
            {
                if ($this->writeFile($this->makeFilename($id),$doc))
                {
                    $this->written_docs++;
                }
                else
                {
                    $this->failed_docs++;
                }
            }

            $i = $this->skipped_docs + $this->written_docs + $this->failed_docs;

            if ($i>0 && $i % 25000 == 0)
            {
                $this->logger->log("processed " . number_format($i) . " documents");
            }
        }
    }
}