<?php

class Names extends BaseClass
{
    private $nsrNames = [];
    private $ttikNames = [];
    private $wikispeciesNames = [];
    private $allNames = [];
    protected $job_name = "names";

    public function __construct ()
    {
        parent::__construct();
        $this->logger->setCallingClassOverride(get_class());
    }

    public function generate()
    {
        $this->getNsrNames();
        $this->getTtikNames();
        $this->getWikispeciesNames();
        $this->groupNames();
        $this->generateJsonDocuments();
        $this->logger->log("finished");
    }

    public function doDeleteExisting()
    {
        if ($this->delete_existing)
        {
            $empty = $this->export_basepath . "empty/";
            mkdir($empty);
            exec("rsync -a --delete " . $empty ." " . $this->export_path, $output, $retval);
            rmdir($empty);
            $this->logger->log("deleted old files; rsync exit code: $retval");
        }
    }

    private function getNsrNames()
    {
        $i=0;
        $this->table_name = "nsr";
        $this->nsrNames = array_map(function($a) use (&$i)
            {
                $c = json_decode($a["common_names"],true);
                array_walk($c,function(&$a) use (&$i) { $a["name"] = strtolower($a["name"]); $i++; });

                return [
                    "scientific_name" => empty($a["nomen"]) ? $a["scientific_name"] : $a["nomen"],
                    "common_names" => $c
                ];
            }, $this->getNames());

        $this->logger->log("got " . number_format($i) . " names for " . number_format(count($this->nsrNames)) . " taxa from NSR");
    }

    private function getTtikNames()
    {
        $i=0;

        $stmt = $this->db->prepare("
            select
                _a.taxon,
                _e.name,
                _f.nametype,
                _e.language_id,
                trim(_e.uninomial || ' ' || ifnull(_e.specific_epithet,'') || ' '|| ifnull(_e.infra_specific_epithet,'')) as nomen

            from
                taxa _a

            left join name_types _f
                on _a.project_id = _f.project_id

            left join names _e
                on _a.project_id = _e.project_id
                and _a.id = _e.taxon_id
                and _e.type_id = _f.id

            where
                _f.nametype in ('isValidNameOf','isAlternativeNameOf','isPreferredNameOf')

            order by
                _a.taxon

        ");

        $result = $stmt->execute();

        while ($res = $result->fetchArray(SQLITE3_ASSOC))
        {
            if ($res["nametype"]=="isValidNameOf")
            {
                $this->ttikNames[$res["taxon"]] = [ "scientific_name" => $res["nomen"] ];
            }
        }

        $result->reset();

        while ($res = $result->fetchArray(SQLITE3_ASSOC))
        {
            if ($res["nametype"]!="isValidNameOf" && $res["language_id"]==$this->ttik_language_id)
            {
                $this->ttikNames[$res["taxon"]]["common_names"][] =
                    [
                        "name" => strtolower($res["name"]),
                        "name_type" => $res["nametype"],
                        "language_code" => "nl"
                    ];
                $i++;
            }
        }

        $this->ttikNames =
            array_filter(
                array_values($this->ttikNames),
                function($a)
                {
                    return isset($a["common_names"]) && count($a["common_names"])>0;
                }
            );

        $this->logger->log("got " . number_format($i) . " names for " . number_format(count($this->ttikNames)) . " taxa from TTIK");
    }

    private function getWikispeciesNames()
    {
        $i=0;
        $this->table_name = "wikispecies";
        $this->wikispeciesNames = array_map(function($a) use (&$i)
            {
                if (strpos($a["common_name"], ",")!==false)
                {
                    $language_code = $a["language_code"];

                    $c = array_map(function($b) use ($language_code,&$i)
                        {
                            $i++;
                            return [ "name" => strtolower(trim($b)), "language_code" => $language_code];
                        },explode(",",$a["common_name"]));
                }
                else
                {
                    $i++;
                    $c = [[ "name" => strtolower($a["common_name"]), "language_code" => $a["language_code"]]];
                }

                return [
                    "scientific_name" => $a["scientific_name"],
                    "common_names" => $c
                ];
            },$this->getNames());

        $this->logger->log("got " . number_format($i) . " names for " . number_format(count($this->wikispeciesNames)) . " taxa from Wikispecies");
    }

    private function getNames()
    {
        $data=[];
        $results = $this->db->query("select * from $this->table_name");

        while ($res= $results->fetchArray(SQLITE3_ASSOC))
        {
            $data[] = $res;
        }

        return $data;
    }

    private function groupNames()
    {
        $this->allNames=[];

        foreach ($this->nsrNames as $val)
        {
            // dutch names only
            $c = array_filter($val["common_names"],function($a)
                {
                    return $a["language_code"]=="nl";
                });


            if (empty($c))
            {
                continue;
            }

            /*
                NSR names always have precedence over TTIK & wikispecies, so in the extraordinary case
                that there are only alternative names from NSR, we simply make the first one the preferred name.
            */
            if (empty(array_filter($c,function($a)
                {
                    return $a["name_type"]=="isPreferredNameOf";
                })))
            {
                $c[0]["name_type"]="isPreferredNameOf";
            };

            $this->allNames[$val["scientific_name"]] = [
                "scientific_name" => $val["scientific_name"],
                "common_names" => $c
            ];
        };


        foreach([$this->ttikNames,$this->wikispeciesNames] as $array)
        {
            foreach($array as $val)
            {
                if (!isset($val["scientific_name"]))
                {
                    continue;
                }

                // // dutch names only --> fixed that earlier
                // $c = array_filter($val["common_names"],function($a)
                //     {
                //         return $a["language_code"]=="nl";
                //     });


                // if name doesn't exist yet (if it wasn't in the NSR)
                if (!isset($this->allNames[$val["scientific_name"]]))
                {
                    // same trick as before to force a preferred name
                    // (TTIK has precedence over wiki, which is forced by the processing order of the two arrays)
                    if (empty(array_filter($val["common_names"],function($a)
                        {
                            return isset($a["name_type"]) && $a["name_type"]=="isPreferredNameOf";
                        })))
                    {
                        $val["common_names"][0]["name_type"]="isPreferredNameOf";
                    };

                    $this->allNames[$val["scientific_name"]] = [
                        "scientific_name" => $val["scientific_name"],
                        "common_names" => $val["common_names"]
                    ];
                }
                // name does exist (was also in NSR), might have extra names, which are all (made) not-preferred
                else
                {
                    $existing_names = array_map(function($a)
                        {
                            return $a["name"];
                        }, $this->allNames[$val["scientific_name"]]["common_names"]);


                    $matches = array_filter($val["common_names"],function($a) use ($existing_names)
                    {
                        return !in_array($a["name"],$existing_names);
                    });

                    if (!empty($matches))
                    {
                        // found extra names, but since NSR takes precedence, we force them to be alternative names
                        array_walk($matches, function(&$a)
                        {
                            $a["name_type"]="isAlternativeNameOf";
                        });

                        $this->allNames[$val["scientific_name"]]["common_names"] =
                            array_merge(
                                $this->allNames[$val["scientific_name"]]["common_names"],$matches);
                    }
                }
            }
        }

        $this->logger->log("made " . number_format(count($this->allNames)) . " name groups");
    }

    protected function generateJsonDocuments()
    {
        // {
        //   "id": "meles_meles",
        //   "created": "2021-05-11T03:14:22+00:00",
        //   "_key": "meles_meles",
        //   "scientific_name": "Meles meles (Linnaeus, 1758)",
        //   "comon_names": [
        //     {
        //       "name": "das",
        //       "language": "nl"
        //     }
        //   ]
        // }

        $now= date("c");
        $i=0;


        //  TAKE NOTE: we might have multiple preferred names (and at least one)!

        foreach($this->allNames as $name)
        {
            // $b = str_replace(" ","_",strtolower($name["scientific_name"]));
            $b = preg_replace('/[^a-zA-Z]+/',"_",strtolower($name["scientific_name"]));
            $doc = [
                "id" => $b,
                "created" => $now,
                "scientific_name" => $name["scientific_name"],
                "common_names" => []
            ];

            $havepref = false;

            foreach ($name["common_names"] as $val)
            {
                if (!isset($val["name"]))
                {
                    continue;
                }

                $doc["common_names"][] = [
                    "name" => $val["name"],
                    "language" => $val["language_code"],
                    "preferred" => (isset($val["name_type"]) && ($val["name_type"] == "isPreferredNameOf") && !$havepref)
                 ];

                 if (isset($val["name_type"]) && ($val["name_type"] == "isPreferredNameOf"))
                 {
                    $havepref = true;
                 }
            }

            if (!$havepref)
            {
                $this->logger->info("taxon without preferred name: " . $name["scientific_name"] ." (skipped)");
                continue;
            }

            if (count($doc["common_names"])==0)
            {
                $this->logger->info("taxon without common names: " . $name["scientific_name"] . " (skipped)");
                continue;
            }

            $f = $this->export_path . $b . ".json";

            if (file_put_contents($f,json_encode($doc)))
            {
                $i++;
            }
            else
            {
                $this->logger->warning("couldn't write $f",1);
            }

        }

        $this->logger->log("wrote " . number_format($i) . " files");
        $this->setJobResult(["files" => $i]);
    }

}