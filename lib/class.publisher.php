<?php

class Publisher extends BaseClass
{
    private $target_basepath;
    private $target_subpath;
    private $target_path;
    private $publish_empty_folders=false;
    private $copied = 0;
    private $failed = 0;
    private $failed_files;
    private $publishing_action;
    protected $job_name = "publish";

    public function __construct ()
    {
        parent::__construct();
        $this->logger->setCallingClassOverride(get_class());
    }

    final function setPublishingAction($publishing_action)
    {
        $this->publishing_action = $publishing_action;
    }

    final function logPublishing()
    {
        if (empty($this->publishing_action))
        {
            return;
        }

        $this->job_type = "publisher";
        $this->job_name = $this->publishing_action;

        $this->setJobResult([
            // "action" => $this->publishing_action,
            "moved to load folder" => $this->copied,
            // "failed" => $this->failed,
            "setting: publish_empty_folders" => ($this->publish_empty_folders ? "y" : "n")
        ]);

        $this->logGeneration();
    }

    final function setTargetPaths($base,$sub)
    {
        $this->target_basepath = $base;
        $this->target_subpath = $sub;

        if (empty($this->target_basepath))
        {
            throw new Exception("document base path not set", 1);
        }
        if (empty($this->target_subpath))
        {
            throw new Exception("document subpath not set", 1);
        }

        $this->target_path = rtrim($this->target_basepath,"/") . "/" . rtrim($this->target_subpath,"/") ."/";

        if (!is_writeable($this->target_path))
        {
            throw new Exception("target path $this->target_path isn't writeable" ,1);
        }

        $this->logger->log("target path: " . $this->target_path);
    }

    final function setPublishEmptyFolders($publish_empty_folders)
    {
        if (is_bool($publish_empty_folders))
        {
            $this->publish_empty_folders = $publish_empty_folders;
            $this->logger->log("publish empty folders: " . ($this->publish_empty_folders ? "y" : "n"));
        }
    }

    final function publish()
    {
        $files = glob($this->export_path . "*.json");

        if (count($files)==0 && !$this->publish_empty_folders)
        {
            $this->logger->log("not publishing empty export path; set PUBLISH_EMPTY_FOLDERS=1 to override (this will delete existing files in load folder)",2);
            return;
        }

        $this->doDeleteExisting();
        $this->failed_files=[];
        $this->copied = $this->failed = 0;

        foreach ($files as $file)
        {
            // if (copy($file,$this->target_path . basename($file)))
            if (rename($file,$this->target_path . basename($file)))
            {
                $this->copied++;
            }
            else
            {
                $this->failed++;
                $this->failed_files = $file;
            }
        }

        $this->logger->log("moved " . $this->copied . "; failed " . $this->failed);
    }

    final function doDeleteExisting()
    {
        if ($this->delete_existing)
        {
            $empty = $this->export_basepath . "empty/";
            mkdir($empty);
            exec("rsync -a --delete " . $empty ." " . $this->target_path, $output, $retval);
            rmdir($empty);
            $this->logger->log("deleted old files; rsync exit code: $retval");
        }
    }
}