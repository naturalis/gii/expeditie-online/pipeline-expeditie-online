<?php

class Subcollections extends BaseClass
{
    private $this_category = "subcollection";
    protected $job_name = "subcollections";

    public function __construct ()
    {
        parent::__construct();
        $this->logger->setCallingClassOverride(get_class());
        $this->setCategory($this->this_category);
    }

    public function generate()
    {
        $this->setLemmaItems();
        $this->setNatuurwijzerColumn("collections");
        $this->setNatuurwijzerLinks();
        $this->generateJsonDocuments();

        $this->logger->log("finished");
    }

    protected function generateJsonDocuments()
    {
        $i = 0;
        $j = 0;

        foreach($this->lemma_data as $key => $lemma)
        {
            $id  = $this->makeDocId($lemma["key"]) . "-" . $this->language;
            $doc = $this->makeDoc($this->category,$this->language,$id,$lemma["key"]);
            $doc = $this->docAddContent($doc,$lemma);

            $l_key = "collectie_" . strtolower($lemma["key"]);

            if (isset($lemma["key"]) && isset($this->links[$l_key]))
            {
                $doc["links"] = $this->links[$l_key];
            }

            if (empty($doc["content"]) && empty($doc["links"]) && empty($doc["multimedia"]) && !$this->generate_empty)
            {
                $j++;
                continue;
            }

            if ($this->writeFile($this->makeFilename($id),$doc))
            {
                $i++;
            }
        }

        $this->logger->log("wrote " . number_format($i) . " files");
        $this->logger->log("skipped " . number_format($j) . " because of lack of content");

        $this->setJobResult([
            "files" => $i,
            "skipped (lack of content)" => $j
        ]);
    }

}