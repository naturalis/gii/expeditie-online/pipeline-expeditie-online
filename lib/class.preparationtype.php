<?php

class PreparationType extends BaseClass
{
    private $this_category = "preparation-type";
    protected $job_name = "preparation-types";

    public function __construct ()
    {
        parent::__construct();
        $this->logger->setCallingClassOverride(get_class());
        $this->setCategory($this->this_category);
    }
}