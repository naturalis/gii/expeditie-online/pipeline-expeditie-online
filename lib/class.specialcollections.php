<?php

class SpecialCollections extends BaseClass
{
    private $this_category = "special-collection";
    protected $job_name = "special-collections";
    private $unit_ids = [];

    public function __construct ()
    {
        parent::__construct();
        $this->logger->setCallingClassOverride(get_class());
        $this->setCategory($this->this_category);
    }

    public function generate()
    {
        $this->setLemmaItems();
        $this->setUnitIds();
        $this->generateJsonDocuments();
        $this->logger->log("finished");
    }

    protected function setUnitIds()
    {
        $stmt = $this->db->prepare("select * from special_collections_ids");
        $result = $stmt->execute();
        $i = 0;
        while ($res = $result->fetchArray(SQLITE3_ASSOC))
        {
            $this->unit_ids[$res["collection"]][] = $res["unitid"];
            $i++;
        }

        $this->logger->log("found " . number_format($i) . " unitids for " . number_format(count($this->unit_ids)) ." special colletions");
    }

    protected function generateJsonDocuments()
    {
        // $this->generateJsonDocuments_ONLY_IF_THERE_IS_CONTENT();
        $this->generateJsonDocuments_EVEN_IF_THERE_ARE_ONLY_UNITIDS();
    }

    protected function generateJsonDocuments_ONLY_IF_THERE_IS_CONTENT()
    {
        $this->logger->log("generating file for special collections with content");

        $i = 0;

        foreach($this->lemma_data as $key => $lemma)
        {
            $id  = $this->makeDocId($lemma["key"]) . "-" . $this->language;
            $doc = $this->makeDoc($this->category,$this->language,$id,$lemma["key"]);
            $doc = $this->docAddContent($doc,$lemma);

            if (isset($lemma["key"]) && isset($this->this->unit_ids[$lemma["key"]]))
            {
                $doc["registration_numbers"] = $this->this->unit_ids[$lemma["key"]];
            }

            if (empty($doc["content"]) && empty($doc["links"]) && empty($doc["multimedia"]) && !$this->generate_empty)
            {
                $j++;
                continue;
            }

            if ($this->writeFile($this->makeFilename($id),$doc))
            {
                $i++;
            }
        }

        $this->logger->log("wrote " . number_format($i) . " files");
        $this->logger->log("skipped " . number_format($j) . " because of lack of content");

        $this->setJobResult([
            "files" => $i,
            "skipped (lack of content)" => $j
        ]);
    }

    protected function generateJsonDocuments_EVEN_IF_THERE_ARE_ONLY_UNITIDS()
    {
        $this->logger->log("generating file for all special collections (even w/o content)");

        $i = 0;
        $j = 0;

        foreach($this->unit_ids as $key => $unit_ids)
        {
            $id  = $this->makeDocId($key) . "-" . $this->language;
            $doc = $this->makeDoc($this->category,$this->language,$id,$key);

            if (isset($this->lemma_data[$key]))
            {
                $doc = $this->docAddContent($doc,$this->lemma_data[$key]);
            }

            $doc["registration_numbers"] = $unit_ids;

            // if (empty($doc["content"]) && empty($doc["links"]) && empty($doc["multimedia"]) && !$this->generate_empty)
            // {
            //     $j++;
            //     continue;
            // }

            if ($this->writeFile($this->makeFilename($id),$doc))
            {
                $i++;
            }
        }

        $this->logger->log("wrote " . number_format($i) . " files");
        $this->setJobResult(["files" => $i]);

       // $this->logger->log("skipped " . number_format($j) . " because of lack of content");
    }
}