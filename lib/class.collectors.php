<?php

class Collectors extends BaseClass
{
    private $this_category = "collector";
    protected $job_name = "collectors";

    public function __construct ()
    {
        parent::__construct();
        $this->logger->setCallingClassOverride(get_class());
        $this->setCategory($this->this_category);
    }

    public function generate()
    {
        $this->setLemmaItems();
        $this->setSynonyms();
        $this->generateJsonDocuments();
        $this->logger->log("finished");
    }

    private function setSynonyms()
    {
        foreach ($this->lemma_data as $key => $val)
        {
            $stmt = $this->db->prepare("select * from collector_synonyms where collector = :collector");
            $stmt->bindValue(':collector',$key,SQLITE3_TEXT);
            $result = $stmt->execute();
            while ($res = $result->fetchArray(SQLITE3_ASSOC))
            {
                // DwCA calculator removes some characters: [],
                // https://gitlab.com/naturalis/bii/nba/naturalis_data_api/-/blob/V2_master/nl.naturalis.nba.dao/src/main/java/nl/naturalis/nba/dao/format/calc/RecordedByCalculator.java#L49
                $synonym = str_replace(["[","]",","], "", $res["synonym"]);
                $this->lemma_data[$key]["synonyms"][] = $synonym;
            }
            $this->logger->log("found " . (isset($this->lemma_data[$key]["synonyms"]) ? count($this->lemma_data[$key]["synonyms"]) : 0 ) . " synonyms for lemma '$key'");
        }
    }

    protected function generateJsonDocuments()
    {

        // {
        //   "category": "collector",
        //   "id": "siebold-nl",
        //   "created": "2021-06-02T05:16:40",
        //   "key": "Von Siebold",
        //   "language": "nl",
        //   "content": [
        //     {
        //       "title": "Philipp Franz von Siebold",
        //       "body": "Jonkheer Philipp Franz Balthasar von Siebold was een van de westerlingen."
        //     }
        //   ],
        //   "synonyms":[
        //       "Siebold, PF von",
        //       "Siebold, Ph.F. von",
        //       "von Siebold"
        //    ]
        // }

        $i = 0;
        $j = 0;

        foreach($this->lemma_data as $key => $lemma)
        {
            $id = $this->makeDocId($lemma["key"]) . "-" . $this->language;
            $doc = $this->makeDoc($this->category,$this->language,$id,$lemma["key"]);
            $doc = $this->docAddContent($doc,$lemma);

            if (isset($lemma["synonyms"]) && count($lemma["synonyms"])>0)
            {
                $doc["synonyms"] = $lemma["synonyms"];
            }

            if (empty($doc["content"]) && empty($doc["links"]) && empty($doc["multimedia"]) && !$this->generate_empty)
            {
                continue;
            }

            if ($this->writeFile($this->makeFilename($id),$doc))
            {
                $i++;
            }
        }

        $this->logger->log("wrote $i files");
        $this->logger->log("skipped " . number_format($j) . " because of lack of content");

         $this->setJobResult([
            "files" => $i,
            "skipped (lack of content)" => $j
        ]);
    }

}