<?php

class Highlights extends BaseClass
{
    private $this_category = "highlight";
    private $query_param = "?standalone";
    protected $job_name = "highlights";

    public function __construct ()
    {
        parent::__construct();
        $this->logger->setCallingClassOverride(get_class());
        $this->setCategory($this->this_category);
    }

    public function generate()
    {
        $this->setTopstukkenQueryParam();
        $this->setLemmaItems();
        $this->generateJsonDocuments();
        $this->logger->log("finished");
    }

    protected function setTopstukkenQueryParam()
    {
        if ($this->topstukken_standalone)
        {
            $this->query_param = "?standalone";
        }
        else
        {
            $this->query_param = "";
        }
    }

    protected function setLemmaItems()
    {
        $stmt = $this->db->prepare("select registrationNumber,title,url from topstukken");
        $result = $stmt->execute();

        while ($res = $result->fetchArray(SQLITE3_ASSOC))
        {
            $this->lemma_data[] = $res;
        }

        $this->logger->log("found " . count($this->lemma_data) . " highlights");
    }

    protected function generateJsonDocuments()
    {
        // {
        //   "category": "highlight",
        //   "id": "L.2110004-nl",
        //   "created": "2021-06-02T05:16:40",
        //   "key": "L.2110004",
        //   "links": [
        //     {
        //       "text": "Apothekersherbarium",
        //       "url": "https://topstukken.naturalis.nl/object/cade-apothekersherbarium"
        //     }
        //   ]
        // }

        $i = $j = 0;

        foreach($this->lemma_data as $key => $lemma)
        {
            $id = $this->makeDocId($lemma["registrationNumber"]);
            $doc = $this->makeDoc($this->category,$this->language,$id,$lemma["registrationNumber"]);
            // unset($doc["language"]);
            $doc = $this->docAddContent($doc,$lemma);
            $doc["links"][] = [ "text" => $lemma["title"], "url" => $lemma["url"] . $this->query_param ];

            if (empty($doc["content"]) && empty($doc["links"]) && empty($doc["multimedia"]) && !$this->generate_empty)
            {
                $j++;
                continue;
            }

            if ($this->writeFile($this->makeFilename($id),$doc))
            {
                $i++;
            }
        }

        $this->logger->log("wrote $i files");
        $this->logger->log("skipped " . number_format($j) . " because of lack of content");

        $this->setJobResult([
            "files" => $i,
            "skipped (lack of content)" => $j
        ]);
    }
}