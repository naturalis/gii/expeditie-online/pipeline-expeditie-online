<?php

class BaseClass
{
    protected $db;
    protected $db_path;
    protected $imported = 0;
    protected $total = 0;
    protected $clear_html = false;
    protected $export_basepath;
    protected $export_subpath;
    protected $export_path;
    protected $delete_existing=true;
    protected $generate_empty=false;
    protected $language;
    protected $ttik_language_id;
    protected $category;
    protected $lemma_data = [];
    protected $links = [];
    protected $now;
    protected $sql_1 = "select * from glossary_synonyms where synonym like '%CATEGORY%|%' and language_id = %LANGUAGE_ID%";
    protected $sql_2 = "select * from glossary where id = %ID% and language_id = %LANGUAGE_ID%";

    protected $natuurwijzer_column;
    protected $natuurwijzer_base_url = "https://natuurwijzer.naturalis.nl";
    protected $natuurwijzer_query_param = "?standalone";
    protected $natuurwijzer_source_add_on = ""; // "" (Natuurwijzer)";
    protected $natuurwijzer_standalone = false;
    protected $topstukken_standalone = false;
    protected $logger;

    protected $job_start;
    public $job_type = 'generate';
    protected $log_table =
        "create table if not exists job_log (
            id             INTEGER PRIMARY KEY  autoincrement,
            job_type       varchar(255) not null,
            job_name       varchar(255) not null,
            job_result     text,
            job_started    timestamp not null,
            job_finished   timestamp not null
        );";

    private $sql_log = "
        insert into job_log (
            job_type, job_name, job_result, job_started, job_finished
        ) values (
            :job_type, :job_name, :job_result, :job_started, datetime('now')
        )";

    protected $job_result=[];

    public function __construct()
    {
        $this->logger = new Logger();

        $this->db_path = getenv('DATABASE_PATH');

        if (empty($this->db_path))
        {
            throw new Exception("empty database path", 1);

        }

        $this->db = new SQLite3($this->db_path);
        $this->db->exec($this->log_table);
        $this->now = date("c");
        $this->job_start = date('Y-m-d H:i:s');
    }

    public function __destruct()
    {
        if ($this->db)
        {
            $this->db->close();
        }
    }

    final function setJobResult($job_result)
    {
        $this->job_result = $job_result;
    }

    final function setExportPaths($base,$sub)
    {
        $this->export_basepath = $base;
        $this->export_subpath = $sub;

        if (empty($this->export_basepath))
        {
            throw new Exception("document base path not set", 1);
        }
        if (empty($this->export_subpath))
        {
            throw new Exception("document subpath not set", 1);
        }

        $this->export_path = rtrim($this->export_basepath,"/") . "/" . rtrim($this->export_subpath,"/") ."/";

        if (!is_writeable($this->export_path))
        {
            throw new Exception("export path $this->export_path isn't writeable" ,1);
        }

        $this->logger->log("export path: " . $this->export_path);
    }

    final function setDeleteExisting($delete_existing)
    {
        if (is_bool($delete_existing))
        {
            $this->delete_existing = $delete_existing;
            $this->logger->log("delete existing: " . ($this->delete_existing ? "y" : "n"));
        }
    }

    public function doDeleteExisting()
    {
        if ($this->delete_existing)
        {
            $del = 0;
            foreach (glob($this->export_path . $this->category . "-*.json") as $filename)
            {
                if (unlink($filename))
                {
                    $del++;
                }
            }
            $this->logger->log("deleted $del old file(s)");
        }
    }

    final function setGenerateEmpty($generate_empty)
    {
        if (is_bool($generate_empty))
        {
            $this->generate_empty = $generate_empty;
            $this->logger->log("generate empty: " . ($this->generate_empty ? "y" : "n"));
        }
    }

    final function setCategory($category)
    {
        $this->category = $category;
        $this->logger->log("category: " . $this->category);
    }

    final function setLanguage($language)
    {
        $this->language = $language;
        $this->logger->log("language: " . $this->language);
    }

    final function setTtikLanguageID($ttik_language_id)
    {
        $this->ttik_language_id = $ttik_language_id;
        $this->logger->log("TTIK glossary language ID: " . $this->ttik_language_id);
    }

    final function setNatuurwijzerStandalone($natuurwijzer_standalone)
    {
        $this->natuurwijzer_standalone = $natuurwijzer_standalone;
        $this->logger->log("Natuurwijzer standalone links: " . ($this->natuurwijzer_standalone ? "y" : "n"));
        $this->setNatuurwijzerQueryParam();
    }

    final function setTopstukkenStandalone($topstukken_standalone)
    {
        $this->topstukken_standalone = $topstukken_standalone;
        $this->logger->log("Topstukken standalone links: " . ($this->topstukken_standalone ? "y" : "n"));
    }

    public function setClearHtml($state)
    {
        if (is_bool($state))
        {
            $this->clear_html = $state;
        }
    }

    public function clearHtml($text)
    {
        return trim($this->clear_html ? $this->manageTags(html_entity_decode($text)) : $text);
    }

    protected function manageTags($text)
    {
        //  removing all markup, except bold & italic + converting to markdown
        $md = $text;
        $md = preg_replace('/<(em|i)>([^<]*)<\/(em|i)>/i', '_$2_', $md);
        // $md = preg_replace('/<(strong|b)>([^<]*)<\/(strong|b)>/i', '**$2**', $md);

        return strip_tags($md);
    }

    public function generate()
    {
        $this->setLemmaItems();
        $this->generateJsonDocuments();
        $this->logger->log("finished");
    }

    final function logGeneration()
    {
        $stmt = $this->db->prepare($this->sql_log);
        $stmt->bindValue(':job_type',$this->job_type,SQLITE3_TEXT);
        $stmt->bindValue(':job_name',$this->job_name,SQLITE3_TEXT);
        $stmt->bindValue(':job_result',json_encode($this->job_result),SQLITE3_TEXT);
        $stmt->bindValue(':job_started',$this->job_start,SQLITE3_TEXT);
        $stmt->execute();
    }

    private function setNatuurwijzerQueryParam()
    {
        if ($this->natuurwijzer_standalone)
        {
            $this->natuurwijzer_query_param = "?standalone";
        }
        else
        {
            $this->natuurwijzer_query_param = "";
        }
    }

    protected function setLemmaItems()
    {

        $stmt = $this->db->prepare(
            str_replace(
                [ '%CATEGORY%','%LANGUAGE_ID%' ],
                [ $this->db->escapeString($this->category),$this->db->escapeString($this->ttik_language_id) ],
                $this->sql_1
            )
        );
        $result = $stmt->execute();

        $this->lemma_data=[];

        while ($res = $result->fetchArray(SQLITE3_ASSOC))
        {
            if (strpos($res["synonym"], "|")===false)
            {
                continue;
            }

            $t = explode("|", $res["synonym"]);
            $key = $t[1];
            $this->lemma_data[$key]["key"] = $key;

            $stmt_2 = $this->db->prepare(
                str_replace(
                    [ '%ID%', '%LANGUAGE_ID%' ],
                    [ $this->db->escapeString($res["glossary_id"]),$this->db->escapeString($this->ttik_language_id) ],
                    $this->sql_2
                )
            );

            $result_2 = $stmt_2->execute();

            while ($res_2 = $result_2->fetchArray(SQLITE3_ASSOC))
            {
                $this->lemma_data[$key]["content"][] = [
                    "title" => $res_2["term"],
                    "language" => $this->language,
                    "content" => $this->clearHtml($res_2["definition"])
                ];

            }
        }

        $this->logger->log("got " . count($this->lemma_data) . " lemmas");
    }

    protected function setNatuurwijzerColumn($column)
    {
        $this->natuurwijzer_column = $column;
    }

    protected function setNatuurwijzerLinks()
    {
        if (empty($this->natuurwijzer_column))
        {
            throw new Exception("natuurwijzer column not set" ,1);
        }

        $stmt = $this->db->prepare("select * from natuurwijzer where " . $this->natuurwijzer_column . " is not null");
        $result = $stmt->execute();

        $i=0;
        while ($res = $result->fetchArray(SQLITE3_ASSOC))
        {
            foreach(json_decode($res[$this->natuurwijzer_column]) as $field)
            {
                $i++;
                $this->links[strtolower($field)][] = [
                    "url" => $this->natuurwijzer_base_url . $res["url"] . $this->natuurwijzer_query_param,
                    "text" => $res["title"] . $this->natuurwijzer_source_add_on
                ];
                    // $res[image_urls]
                    // $res[author]
                    // $res[intro_text]
            }
        }

        $this->logger->log(
            "found " . number_format($i) .
            " links from Natuurwijzer for " . number_format(count($this->links)) . " " . $this->natuurwijzer_column . " values");
    }

    protected function makeDocId($key)
    {
        return preg_replace('/[^a-zA-Z0-9]+/',"_",strtolower($key));
    }

    protected function makeDoc($category,$language,$id,$key)
    {
        $doc = [
            "category" => str_replace("-", "_", $category),
            "id" => $id,
            "created" => $this->now,
            "key" => $key,
            "language" => $language
        ];

        return $doc;
    }

    protected function makeFilename($id)
    {
        return $this->export_path . $this->category . "-" .  $id . ".json";
    }

    protected function writeFile($filename,$doc)
    {
        if (file_put_contents($filename,json_encode($doc)))
        {
            return true;
        }
        else
        {
            $this->logger->warning("couldn't write $f",1);
            return false;
        }
    }

    protected function docAddContent($doc,$lemma)
    {
        if (isset($lemma["content"]) && count($lemma["content"])>0)
        {
            $doc["content"] = array_map(
                function($a)
                {
                    $b=[];

                    if (isset($a["title"]))
                    {
                        $b["title"] = $a["title"];
                    }

                    if (isset($a["content"]))
                    {
                        $b["body"] = $a["content"];
                    }

                     return $b;
                },$lemma["content"]);
        }

        if (isset($doc["content"][0]["title"]))
        {
            $doc["title"] = $doc["content"][0]["title"];
        }

        return $doc;
    }

    protected function generateJsonDocuments()
    {
        $i = 0;
        $j = 0;

        foreach($this->lemma_data as $key => $lemma)
        {
            $id  = $this->makeDocId($lemma["key"]) . "-" . $this->language;
            $doc = $this->makeDoc($this->category,$this->language,$id,$lemma["key"]);
            $doc = $this->docAddContent($doc,$lemma);

            if (empty($doc["content"]) && empty($doc["links"]) && empty($doc["multimedia"]) && !$this->generate_empty)
            {
                $j++;
                continue;
            }

            if ($this->writeFile($this->makeFilename($id),$doc))
            {
                $i++;
            }
        }

        $this->logger->log("wrote " . number_format($i) . " files");
        $this->logger->log("skipped " . number_format($j) . " because of lack of content");

        $this->setJobResult([
            "files" => $i,
            "skipped (lack of content)" => $j
        ]);
    }

    protected function getLastJsonError()
    {
        $json_error = false;

        switch (json_last_error())
        {
            case JSON_ERROR_NONE:
            break;
            case JSON_ERROR_DEPTH:
                $json_error = 'Maximum stack depth exceeded';
            break;
            case JSON_ERROR_STATE_MISMATCH:
                $json_error = 'Underflow or the modes mismatch';
            break;
            case JSON_ERROR_CTRL_CHAR:
                $json_error = 'Unexpected control character found';
            break;
            case JSON_ERROR_SYNTAX:
                $json_error = 'Syntax error, malformed JSON';
            break;
            case JSON_ERROR_UTF8:
                $json_error = 'Malformed UTF-8 characters, possibly incorrectly encoded';
            break;
            default:
                $json_error = 'Unknown error';
            break;
        }

        return $json_error;
    }

}