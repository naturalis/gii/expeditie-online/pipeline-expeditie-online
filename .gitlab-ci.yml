variables:
  TRIVY_DB_REPOSITORY: "aquasec/trivy-db:2"

stages:
  - check
  - build
  - test
  - publish

include:
  - template: Security/Secret-Detection.gitlab-ci.yml

secret_detection:
  extends: .secret-analyzer
  stage: check
  variables:
    SECRET_DETECTION_HISTORIC_SCAN: "false"
    SECRET_DETECTION_REPORT_FILE: "gl-secret-detection-report.json"
  artifacts:
    reports:
      secret_detection: $SECRET_DETECTION_REPORT_FILE
    expire_in: 7 days
  rules:
    - if: $CI_PIPELINE_SOURCE == "push"
    - if: $CI_PIPELINE_SOURCE == "schedule" && $CI_COMMIT_BRANCH == "develop"
    - if: $CI_PIPELINE_SOURCE == "trigger" && $CI_COMMIT_TAG == null
    - if: $CI_PIPELINE_SOURCE == "web" && $CI_COMMIT_TAG == null && $CI_COMMIT_BRANCH == null
  allow_failure: false
  before_script:
    - apk add --no-cache jq
  script:
    - /analyzer run
    - |
      if [ -f "$SECRET_DETECTION_REPORT_FILE" ]; then
        if [ "$(jq ".vulnerabilities | length" $SECRET_DETECTION_REPORT_FILE)" -gt 0 ]; then
          echo "Vulnerabilities detected!"
          echo "Please check the $SECRET_DETECTION_REPORT_FILE security file."
          echo "Or run gitleak locally on your repository."
          exit 1
        fi
      else
        echo "Artifact $SECRET_DETECTION_REPORT_FILE does not exist."
        echo "No evaluation can be performed."
      fi

Haskell Dockerfile linter:
  stage: check
  rules:
    - if: $CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_TAG == null
    - if: $CI_PIPELINE_SOURCE == "schedule" && $CI_COMMIT_BRANCH == "develop"
    - if: $CI_PIPELINE_SOURCE == "web" && $CI_COMMIT_TAG == null && $CI_COMMIT_BRANCH == null
  image: hadolint/hadolint:latest-debian
  script:
    - hadolint docker/Dockerfile

Shellcheck:
  stage: check
  rules:
    - if: $CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_TAG == null
    - if: $CI_PIPELINE_SOURCE == "schedule" && $CI_COMMIT_BRANCH == "develop"
    - if: $CI_PIPELINE_SOURCE == "web" && $CI_COMMIT_TAG == null && $CI_COMMIT_BRANCH == null
  image: koalaman/shellcheck-alpine:stable
  script:
    - shellcheck docker/*.sh

Image build:
  stage: build
  rules:
    - if: $CI_PIPELINE_SOURCE == "push"
    - if: $CI_PIPELINE_SOURCE == "trigger" && $CI_COMMIT_TAG == null
    - if: $CI_PIPELINE_SOURCE == "schedule" && $CI_COMMIT_BRANCH == "develop"
    - if: $CI_PIPELINE_SOURCE == "web" && $CI_COMMIT_TAG == null && $CI_COMMIT_BRANCH == null
  image: docker:20
  services:
    - docker:20-dind
  before_script:
    - echo -n $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
  script:
    - source docker/export_cache_tags.sh
    - docker/build_and_push.sh

Trivy vulnerability scanner:
  stage: test
  rules:
    - if: $CI_PIPELINE_SOURCE == "push"
    - if: $CI_PIPELINE_SOURCE == "schedule" && $CI_COMMIT_BRANCH == "develop"
    - if: $CI_PIPELINE_SOURCE == "trigger" && $CI_COMMIT_TAG == null
    - if: $CI_PIPELINE_SOURCE == "web" && $CI_COMMIT_TAG == null && $CI_COMMIT_BRANCH == null
  image:
    name: docker.io/aquasec/trivy:latest
    entrypoint: [""]
  variables:
    GIT_STRATEGY: none
    TRIVY_USERNAME: "$CI_REGISTRY_USER"
    TRIVY_PASSWORD: "$CI_REGISTRY_PASSWORD"
    TRIVY_AUTH_URL: "$CI_REGISTRY"
  cache:
    key: trivy-cache
    paths:
      - .trivy/
  before_script:
    - "wget --header \"PRIVATE-TOKEN: $TRIVYIGNORE_ACCESS_TOKEN\" \"https://gitlab.com/api/v4/projects/41791412/repository/files/trivyignore/raw?ref=main\" --output-document=.trivyignore"
    - trivy --version
    - mkdir -p .trivy
    - |
      if [ ! -f .trivy/db/trivy.db ] || [ $(find .trivy/ -name "trivy.db" -mtime +1 | grep "trivy.db") ]; then
          echo "Download the trivy db!";
          rm -rf .trivy/db;
          trivy image --cache-dir .trivy/ --download-db-only --db-repository $TRIVY_DB_REPOSITORY;
      fi
  script:
    - trivy image --removed-pkgs $CI_REGISTRY_IMAGE:build_$CI_COMMIT_REF_SLUG
    - trivy image --severity HIGH,CRITICAL --ignore-unfixed --exit-code 1 --removed-pkgs $CI_REGISTRY_IMAGE:build_$CI_COMMIT_REF_SLUG
    
Image publish:
  stage: publish
  rules:
    - if: $CI_PIPELINE_SOURCE == "push"
    - if: $CI_PIPELINE_SOURCE == "schedule" && $CI_COMMIT_BRANCH == "develop"
    - if: $CI_PIPELINE_SOURCE == "trigger" && $CI_COMMIT_TAG == null
    - if: $CI_PIPELINE_SOURCE == "web" && $CI_COMMIT_TAG == null && $CI_COMMIT_BRANCH == null
  image: docker:20
  services:
    - docker:20-dind
  before_script:
    - echo -n $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
  script:
    - docker/publish.sh

