<?php

   $action = getenv('PIPELINE_ACTION');

   if (is_null($action))
   {
      exit(0);
   }

   include_once("lib/class.logger.php");
   include_once("lib/class.baseClass.php");
   include_once("lib/class.subcollections.php");
   include_once("lib/class.typestatuses.php");
   include_once("lib/class.preparationtype.php");
   include_once("lib/class.collectors.php");
   include_once("lib/class.taxa.php");
   include_once("lib/class.highlights.php");
   include_once("lib/class.specialcollections.php");
   include_once("lib/class.names.php");

   $language = empty(getenv('DOCUMENT_LANGUAGE')) ? "nl" : getenv('DOCUMENT_LANGUAGE');
   $ttik_language_id = ($language!='nl' ? 26 : 24);

   $path = getEnv('DOCUMENT_PATH_PREVIEW');
   $subpath_general = getEnv('DOCUMENT_SUBPATH_GENERAL');
   $subpath_taxa = getEnv('DOCUMENT_SUBPATH_TAXA');
   $subpath_names = getEnv('DOCUMENT_SUBPATH_NAMES');

   $delete_existing = empty(getenv('DELETE_EXISTING_DOCS')) ? true : getenv('DELETE_EXISTING_DOCS')!="0";

   $document_limit = empty(getenv('DOCUMENT_LIMIT')) ? -1 : getenv('DOCUMENT_LIMIT');
   $nba_taxa_limit = empty(getenv('NBA_TAXA_LIMIT')) ? -1 : getenv('NBA_TAXA_LIMIT');

   $natuurwijzer_standalone = empty(getenv('STANDALONE_NATUURWIJZER_LINKS')) ? false : getenv('STANDALONE_NATUURWIJZER_LINKS')=="1";
   $topstukken_standalone = empty(getenv('STANDALONE_TOPSTUKKEN_LINKS')) ? false : getenv('STANDALONE_NATUURWIJZER_LINKS')=="1";

   try {

      $logger = new Logger();

      if (empty($path))
      {
         throw new Exception("missing setting: DOCUMENT_PATH_PREVIEW");
      }

      if (empty($subpath_general))
      {
         throw new Exception("missing setting: DOCUMENT_SUBPATH_GENERAL");
      }

      if (empty($subpath_taxa))
      {
         throw new Exception("missing setting: DOCUMENT_SUBPATH_TAXA");
      }

      if (empty($subpath_names))
      {
         throw new Exception("missing setting: DOCUMENT_SUBPATH_NAMES");
      }

      switch ($action)
      {
         case "generate_taxa":
            $c = new Taxa();
            $c->setExportPaths($path,$subpath_taxa);
            $c->setDocumentLimit($document_limit);
            $c->setNbaTaxaLimit($nba_taxa_limit);
            break;

         case "generate_subcollections":
            $c = new Subcollections();
            $c->setExportPaths($path,$subpath_general);
            break;

         case "generate_typestatus":
            $c = new TypeStatuses();
            $c->setExportPaths($path,$subpath_general);
            break;

         case "generate_preparationtype":
            $c = new PreparationType();
            $c->setExportPaths($path,$subpath_general);
            break;

         case "generate_collectors":
            $c = new Collectors();;
            $c->setExportPaths($path,$subpath_general);
            break;

         case "generate_highlights":
            $c = new Highlights();
            $c->setExportPaths($path,$subpath_general);
            break;

         case "generate_special_collections":
            $c = new SpecialCollections();
            $c->setExportPaths($path,$subpath_general);
            $c->setGenerateEmpty(false);
            break;

         case "generate_names":
            $c = new Names();
            $c->setExportPaths($path,$subpath_names);
            break;

         default:
            throw new Exception("unknown pipeline action: $action", 1);
            break;
      }

      $c->logger->setFile(getenv('LOGFILE_PATH'));

      $c->setClearHtml(true);
      $c->setLanguage($language);
      $c->setTtikLanguageID($ttik_language_id);
      $c->setDeleteExisting($delete_existing);
      $c->setNatuurwijzerStandalone($natuurwijzer_standalone);
      $c->setTopstukkenStandalone($topstukken_standalone);
      $c->doDeleteExisting();
      $c->generate();
      $c->logGeneration();

   }
   catch (Exception $e)
   {
      $logger->setFile(getenv('LOGFILE_PATH'));
      $logger->setCallingClassOverride("Pipeline");
      $logger->log($e->getMessage(),1);
   }
