<?php

   $action = getenv('PIPELINE_ACTION');

   if (is_null($action))
   {
      exit(0);
   }

   include_once("lib/class.logger.php");
   include_once("lib/class.baseClass.php");
   include_once("lib/class.publisher.php");

   $path_preview = getEnv('DOCUMENT_PATH_PREVIEW');
   $path_load = getEnv('DOCUMENT_PATH_LOAD');
   $subpath_general = getEnv('DOCUMENT_SUBPATH_GENERAL');
   $subpath_taxa = getEnv('DOCUMENT_SUBPATH_TAXA');
   $subpath_names = getEnv('DOCUMENT_SUBPATH_NAMES');
   $publish_empty_folders = getEnv('PUBLISH_EMPTY_FOLDERS',false)==="1";

   try {

      $logger = new Logger();

      if (empty($path_preview)) throw new Exception("missing setting: DOCUMENT_PATH_PREVIEW");
      if (empty($path_load)) throw new Exception("missing setting: DOCUMENT_PATH_LOAD");
      if (empty($subpath_general)) throw new Exception("missing setting: DOCUMENT_SUBPATH_GENERAL");
      if (empty($subpath_taxa)) throw new Exception("missing setting: DOCUMENT_SUBPATH_TAXA");
      if (empty($subpath_names)) throw new Exception("missing setting: DOCUMENT_SUBPATH_NAMES");

      $c = new Publisher();
      $c->logger->setFile(getenv('LOGFILE_PATH'));
      $c->setPublishEmptyFolders($publish_empty_folders);

      switch ($action)
      {
         case "publish_taxa":
            $c->setExportPaths($path_preview,$subpath_taxa);
            $c->setTargetPaths($path_load,$subpath_taxa);
            $c->publish();
            $c->setPublishingAction("publish_taxa");
            $c->logPublishing();
            break;

         case "publish_names":
            $c->setExportPaths($path_preview,$subpath_names);
            $c->setTargetPaths($path_load,$subpath_names);
            $c->publish();
            $c->setPublishingAction("publish_names");
            $c->logPublishing();
            break;

         case "publish_general":
            $c->setExportPaths($path_preview,$subpath_general);
            $c->setTargetPaths($path_load,$subpath_general);
            $c->publish();
            $c->setPublishingAction("publish_general");
            $c->logPublishing();
            break;

         case "publish_all":
            $c->setExportPaths($path_preview,$subpath_taxa);
            $c->setTargetPaths($path_load,$subpath_taxa);
            $c->publish();
            $c->setPublishingAction("publish_taxa");
            $c->logPublishing();

            $c->setExportPaths($path_preview,$subpath_names);
            $c->setTargetPaths($path_load,$subpath_names);
            $c->publish();
            $c->setPublishingAction("publish_names");
            $c->logPublishing();

            $c->setExportPaths($path_preview,$subpath_general);
            $c->setTargetPaths($path_load,$subpath_general);
            $c->publish();
            $c->setPublishingAction("publish_general");
            $c->logPublishing();
            break;

         default:
            throw new Exception("unknown pipeline action: $action", 1);
            break;
      }
   }
   catch (Exception $e)
   {
      $logger->setFile(getenv('LOGFILE_PATH'));
      $logger->setCallingClassOverride("Publisher");
      $logger->log($e->getMessage(),1);
   }
